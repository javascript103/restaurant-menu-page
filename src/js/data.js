export const DISHES_TYPE = {
  SOUP: 'Soup',
  HOT_PLATE: 'Hot plate',
  SALAD: 'Salad',
};

// Dummy data
export const menu = [
  {
    type: DISHES_TYPE.SOUP,
    name: 'Ukrainian red borscht',
    imageUrl: 'assets/images/ukrainian_red_borscht.png',
    weight: 260,
    price: 12.5,
  },

  {
    type: DISHES_TYPE.SOUP,
    name: 'Cold beet soup',
    imageUrl: 'assets/images/cold_beet_soup.png',
    weight: 250,
    price: 13.3,
  },

  {
    type: DISHES_TYPE.SOUP,
    name: 'Sorrel soup',
    imageUrl: 'assets/images/sorrel_soup.png',
    weight: 220,
    price: 11.4,
  },

  {
    type: DISHES_TYPE.HOT_PLATE,
    name: 'Fish cutlet',
    imageUrl: 'assets/images/fish_cutlet.png',
    weight: 200,
    price: 13.8,
  },

  {
    type: DISHES_TYPE.HOT_PLATE,
    name: 'Persimmon chicken',
    imageUrl: 'assets/images/persimmon_chicken.png',
    weight: 230,
    price: 14.5,
  },

  {
    type: DISHES_TYPE.SALAD,
    name: 'Caesar salad',
    imageUrl: 'assets/images/caesar_salad.png',
    weight: 210,
    price: 14.5,
  },

  {
    type: DISHES_TYPE.SALAD,
    name: 'Salad with avocado',
    imageUrl: 'assets/images/salad_with_avocado.png',
    weight: 210,
    price: 11.7,
  },

  {
    type: DISHES_TYPE.SALAD,
    name: 'Russian salad',
    imageUrl: 'assets/images/traditional-russian-salad-olivier.png',
    weight: 240,
    price: 15.4,
  },

  {
    type: DISHES_TYPE.SALAD,
    name: 'Greek salad',
    imageUrl: 'assets/images/greek_salad.png',
    weight: 230,
    price: 15.9,
  },

  {
    type: DISHES_TYPE.SOUP,
    name: 'Turkish soup with spices',
    imageUrl: 'assets/images/turkish-soup-with-spices.png',
    weight: 240,
    price: 12.4,
  },

  {
    type: DISHES_TYPE.SOUP,
    name: 'Soup solyanka russian',
    imageUrl: 'assets/images/soup_solyanka_russian.png',
    weight: 240,
    price: 13.2,
  },

  {
    type: DISHES_TYPE.SOUP,
    name: 'Lentil soup',
    imageUrl: 'assets/images/lentil_soup.png',
    weight: 230,
    price: 14.1,
  },

  {
    type: DISHES_TYPE.HOT_PLATE,
    name: 'Meatballs in sauce',
    imageUrl: 'assets/images/meatballs_in_sauce.png',
    weight: 220,
    price: 17.4,
  },

  {
    type: DISHES_TYPE.HOT_PLATE,
    name: 'Khachapuri in adjarian',
    imageUrl: 'assets/images/khachapuri.png',
    weight: 210,
    price: 11.4,
  },

  {
    type: DISHES_TYPE.HOT_PLATE,
    name: 'Roasted meat chop',
    imageUrl: 'assets/images/roasted_meat_chop.png',
    weight: 250,
    price: 16.1,
  },

  {
    type: DISHES_TYPE.SALAD,
    name: 'Salad with bacon',
    imageUrl: 'assets/images/salad_with_bacon.png',
    weight: 210,
    price: 13.9,
  },

  {
    type: DISHES_TYPE.SALAD,
    name: 'Vegetable salad',
    imageUrl: 'assets/images/vegetable_salad.png',
    weight: 220,
    price: 11.5,
  },

  {
    type: DISHES_TYPE.HOT_PLATE,
    name: 'Вumplings with meat',
    imageUrl: 'assets/images/dumplings.jpg',
    weight: 220,
    price: 11.5,
  },

];
