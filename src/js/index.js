import '../styles/reset.css';
import '../styles/style.scss';

import { menu } from './data';

const menuButtons = document.querySelector('.main-header__list');
const menuList = document.querySelector('.menu');

function createMenuCard(cardData) {
  const cardHTML = `
    <div class="menu-card">
      <img class="menu-card__image" src="${cardData.imageUrl}">
      <div class="menu-card__wrapper">
        <div class="menu-card__title">${cardData.name}</div>
        <div class="menu-card__weight">${cardData.weight} gram</div>
      </div>
      <div class="menu-card__price">$ ${cardData.price}</div>
    </div>
  `;
  return cardHTML;
}

function renderCardList(cardList) {
  const cardListHTML = cardList.map((card) => createMenuCard(card));
  menuList.insertAdjacentHTML('afterbegin', cardListHTML.join().replace(/,/g, ''));
}

function clearCardList() {
  menuList.innerHTML = '';
}

function changeMenuActiveItem(element) {
  document.querySelector('.main-header__list-item.active').classList.remove('active');
  element.classList.add('active');
}

function filterMenuByDishesType(event) {
  changeMenuActiveItem(event.target);
  const { type } = event.target.dataset;
  const filteredMenu = menu.filter((card) => card.type === type);
  const renderMenu = filteredMenu.length > 0 ? filteredMenu : menu;
  clearCardList();
  renderCardList(renderMenu);
}

function app() {
  renderCardList(menu);
  menuButtons.addEventListener('click', filterMenuByDishesType);
}

app();
